FROM docker:27.3

# Packages install
RUN apk add --no-cache composer \
                       php83-bcmath \
                       php83-ctype \
                       php83-curl \
                       php83-dom \
                       php83-fileinfo \
                       php83-fpm \
                       php83-gd \
                       php83-iconv \
                       php83-intl \
                       php83-mbstring \
                       php83-opcache \
                       php83-openssl \
                       php83-pecl-mongodb \
                       php83-phar \
                       php83-posix \
                       php83-session \
                       php83-simplexml \
                       php83-tokenizer \
                       php83-xml \
                       php83-xmlwriter \
                       php83-zip
